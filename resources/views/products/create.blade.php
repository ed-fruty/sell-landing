@extends('layouts.container')

@section('heading')
Create a new product
@endsection

@section('panel')
<form method="post" action="{{ route('products.store') }}" enctype="multipart/form-data">
		{{ csrf_field() }}

		<div class="form-group">
    		<label for="name">Name</label>
    		<input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="Enter the name value">
		</div>
		<div class="form-group">
		    <label for="description">Description</label>
		    <textarea class="form-control" id="description" name="description" placeholder="Enter the description value">{{ old('description') }}</textarea>
		</div>
		<div class="form-group">
			<label for="category">Category</label>
			<select name="category_id" class="form-control" id="category_id">
			@foreach($categories as $id => $name)
				<option value="{{ $id }}">{{ $name }}</option>
			@endforeach
			</select>
		</div>
		<div class="form-group">
		    <label for="price">Price</label>
		    <input type="text" class="form-control" id="price" name="price" value="{{ old('price') }}" placeholder="Enter the price value. Example: 3.14">
		</div>
		<div class="form-group">
			<label for="currency">Currency</label>
			<select name="currency" class="form-control" id="currency">
			@foreach($currencies as $id => $name)
				<option value="{{ $id }}">{{ $name }}</option>
			@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="status">Status</label>
			<select name="status" class="form-control" id="status">
			@foreach($status->dropdown() as $id => $name)
				<option value="{{ $id }}">{{ $name }}</option>
			@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="unit">unit</label>
			<select name="unit" class="form-control" id="unit">
				@foreach($unit->dropdown() as $id => $name)
					<option value="{{ $id }}">{{ $name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
		    <label for="image">Image <small>(only jpg/gif/png extensions are supported)</small></label>
		    <input type="file" class="form-control" id="image" name="image" value="{{ old('image') }}" placeholder="Enter the image value">
		</div>
		<div class="form-group">
		    <input type="submit" class="btn btn-info btn-block btn-sm" value="Create">
		</div>
</form>		
@endsection
@extends('layouts.container')

@section('heading')
Products
@endsection

@section('panel')

<div class="row" style="margin-bottom: 10px">
	<div class="col-md-6">
		<a href="{{ route('products.create') }}" class="btn btn-info btn-block btn-sm">Create a new product</a>
	</div>
</div>
<div>
@if($products->isEmpty())
	<div class="alert alert-warning">
		No available products was found.
	</div>
@else
<div class="row">
	<div class="col-md-12">
	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Category</th>
				<th>Image</th>
				<th>Status</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			@foreach($products as $product)
			<tr>
				<td>{{ $product->id }}</td>
				<td>{{ $product->name }}</td>
				<td>{{ $product->category->name }}</td>
				<td>
					@if($product->image)
						<img src="{{ $product->image->getFullPath() }}" width="120" height="100">
					@else
						None
					@endif
				</td>
				<td>
					{{ $status->getStatusName($product->status) }}
				</td>
				<td>
						<form action="{{ route('products.destroy', $product->id) }}" method="post">
							{{ csrf_field() }}
							<input type="hidden" name="_method" value="DELETE">
							
							<div class="btn-group">
								<a href="{{ route('products.edit', $product->id) }}" class="btn btn-warning btn-sm">Edit</a>
								<input type="submit" name="submit" value="Delete" class="btn btn-danger btn-sm" 
									onclick="if (! confirm('Destroy product?')) return false">
							</div>
						</form>
					
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	</div>
</div>
<div class="row">
	<div class="col-md-offset-5">
		{{ $products->links() }}
	</div>
</div>
@endif
@endsection
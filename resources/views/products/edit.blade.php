@extends('layouts.container')

@section('heading')
Edit product "{{ $product->name }}"
@endsection

@section('panel')
<form method="post" action="{{ route('products.update', $product->id) }}" enctype="multipart/form-data">
		{{ csrf_field() }}
		<input type="hidden" name="_method" value="PUT">

		<div class="form-group">
    		<label for="name">Name</label>
    		<input type="text" class="form-control" id="name" name="name" value="{{ $product->name }}" placeholder="Enter the name value">
		</div>
		<div class="form-group">
		    <label for="description">Description</label>
		    <textarea class="form-control" id="description" name="description" placeholder="Enter the description value">{{ $product->description }}</textarea>
		</div>
		<div class="form-group">
			<label for="category">Category</label>
			<select name="category_id" class="form-control" id="category_id">
			@foreach($categories as $id => $name)
				<option value="{{ $id }}" {{ $product->category_id === $id ? 'selected' : '' }} >{{ $name }}</option>
			@endforeach
			</select>
		</div>
		<div class="form-group">
		    <label for="price">Price</label>
		    <input type="text" class="form-control" id="price" name="price" value="{{ $product->price }}" placeholder="Enter the price value. Example: 3.14">
		</div>
		<div class="form-group">
			<label for="currency">Currency</label>
			<select name="currency" class="form-control" id="currency">
			@foreach($currencies as $id => $name)
				<option value="{{ $id }}">{{ $name }}</option>
			@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="status">Status</label>
			<select name="status" class="form-control" id="status">
			@foreach($status->dropdown() as $id => $name)
				<option value="{{ $id }}" {{ $product->status === $id ? 'selected' : '' }} >{{ $name }}</option>
			@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="unit">Unit</label>
			<select name="unit" class="form-control" id="unit">
				@foreach($unit->dropdown() as $id => $name)
					<option value="{{ $id }}" {{ $product->unit === $id ? 'selected' : '' }} >{{ $name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
		    <label for="image">Image <small>(only jpg/gif/png extensions are supported)</small></label>
		    <input type="file" id="image" name="image">

		    @if($product->image)
		    	<div style="margin:5px">
		    		<img class="img-rounded" src="{{ $product->image->getFullPath() }}" width="120" height="100">
		    	</div>
		    @endif
		</div>
		<div class="form-group">
		    <input type="submit" class="btn btn-info btn-block btn-sm" value="Save">
		</div>
</form>		
@endsection
@extends('layouts.container')

@section('heading')
Edit order "{{ $order->id }}"
@endsection

@section('panel')
<form method="post" action="{{ route('orders.update', $order->id) }}" enctype="multipart/form-data">
		{{ csrf_field() }}
		<input type="hidden" name="_method" value="PUT">
<div class="form-group">
		    <label for="phone">Phone</label>
		    <input type="text" class="form-control" id="phone" name="phone" value="{{ $order->phone }}" placeholder="Enter the phone value">
		</div>
		<div class="form-group">
		    <label for="fio">FIO</label>
		    <input type="text" class="form-control" id="fio" name="fio" value="{{ $order->fio }}" placeholder="Enter the fio value">
		</div>
		<div class="form-group">
		    <label for="comment">Comment</label>
		    <textarea class="form-control" id="comment" name="comment" placeholder="Enter the comment value">{{ $order->comment }}</textarea>
		</div>
		<div class="form-group">
			<label for="product_id">Product</label>
			<select name="product_id" class="form-control" id="product_id">
			@foreach($products as $id => $name)
				<option value="{{ $id }}" {{ $order->product_id === $id ? 'selected' : '' }}>{{ $name }}</option>
			@endforeach
			</select>
		</div>
		<div class="form-group">
		    <label for="amount">Amount</label>
		    <input type="text" class="form-control" id="amount" name="amount" value="{{ $order->amount }}" placeholder="Enter the amount value. Example: 0.5">
		</div>
		
		<div class="form-group">
			<label for="status">Status</label>
			<select name="status" class="form-control" id="status">
			@foreach($status->dropdown() as $id => $name)
				<option value="{{ $id }}" {{ $order->status === $id ? 'selected' : '' }}>{{ $name }}</option>
			@endforeach
			</select>
		</div>

	@foreach($order->products as $product)
		<div class="form-group">
			<div class="row product-block">
				<div class="col-md-6">
					<div class="form-group">
						<label>Product</label>
						<select name="products[]" class="form-control">
							@foreach($products as $id => $name)
								<option {{ $id === $product->id ? 'selected' : '' }} value="{{ $id }}">{{ $name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Amount</label>
						<input type="text" name="amounts[]" value="{{ $product->pivot->amount }}" class="form-control" placeholder="1">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label></label>
						<a href="#" class="btn btn-warning btn-remove">Delete</a>
					</div>

				</div>
			</div>
		</div>
	@endforeach
		<div class="form-group">
			<button class="btn btn-primary btn-clone">+ Add a new one</button>
		</div>

		<div class="form-group">
		    <input type="submit" class="btn btn-info btn-block btn-sm" value="Save">
		</div>
</form>		
@endsection
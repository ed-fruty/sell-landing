@extends('layouts.container')

@section('heading')
Orders
@endsection

@section('panel')

<div class="row" style="margin-bottom: 10px">
	<div class="col-md-6">
		<a href="{{ route('orders.create') }}" class="btn btn-info btn-block btn-sm">Create a new order</a>
	</div>
</div>
<div>

<form action="{{ route('orders.index') }}">
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
			    <label for="phone">Phone</label>
			    <input type="text" class="form-control" id="phone" name="phone" value="{{ $request->get('phone') }}" placeholder="Enter the phone value">
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
			    <label for="fio">FIO</label>
			    <input type="text" class="form-control" id="fio" name="fio" value="{{ $request->get('fio') }}" placeholder="Enter the fio value">
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="product_id">Product</label>
				<select name="product_id" class="form-control" id="product_id">
					<option value="">All</option>
				@foreach($products as $id => $name)
					<option value="{{ $id }}" {{ $request->has('product_id') && $request->get('product_id') == $id ? 'selected' : '' }} >{{ $name }}</option>
				@endforeach
				</select>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="status">Status</label>
				<select name="status" class="form-control" id="status">
					<option value="">All</option>
				@foreach($status->dropdown() as $id => $name)
					<option value="{{ $id }}" {{ $request->has('status') && $request->get('status') == $id ? 'selected' : '' }}>{{ $name }}</option>
				@endforeach
				</select>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<button type="submit" class="btn btn-success btn-sm btn-block">Search</button>
		</div>
		<div class="col-md-6">
			<a href="{{ route('orders.index') }}" class="btn btn-warning btn-sm btn-block">Reset</a>
		</div>
	</div>
</form>

@if($orders->isEmpty())
	<div class="alert alert-warning">
		No available orders was found.
	</div>
@else
	<div class="row">
		<div class="col-md-12">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>ID</th>
					<th>Phone</th>
					<th>FIO</th>
					<th>Products</th>
					<th>Status</th>
					<th>Created</th>
					<th>Updated</th>
					<th>Comment</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				@foreach($orders as $order)
				<tr>
					<td>{{ $order->id }}</td>
					<td>{{ $order->phone }}</td>
					<td>{{ $order->fio }}</td>
					<td>
						<table class="table">
							<thead>
								<tr>
									<th>Product name</th>
									<th>Unit price</th>
									<th>Amount</th>
									<th>Summary</th>
								</tr>
							</thead>
							<tbody>
								@foreach($order->products as $product)
									<tr>
										<td>
											<a href="{{ route('products.edit', $product->id) }}" target="_blank">
												{{ $product->name }}
											</a>
										</td>
										<td>{{ $product->price }} {{ $product->currency }}</td>
										<td>{{ $product->pivot->amount }}</td>
										<td>
											{{ $product->getOrderPrice(true) }}
										</td>
									</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<td colspan="3"><strong>Total</strong></td>
									<td>
										@foreach($order->getProductsPrice()->total() as $currency => $price)
											<strong class="text-danger">{{ $price }} {{ $currency }}</strong> <br>
										@endforeach
									</td>
								</tr>
							</tfoot>
						</table>
						{{--<a href="{{ route('products.edit', $order->product_id) }}" target="_blank">--}}
							{{--{{ $order->product->name }}--}}
						{{--</a>--}}
					</td>
					<td>
						{{ $status->getStatusName($order->status) }}
					</td>
					<td>{{ $order->created_at }}</td>
					<td>{{ $order->updated_at }}</td>
					<td>{!! wordwrap(strip_tags($order->comment), 20, '<br>') !!}</td>
					<td>
							<a href="{{ route('orders.edit', $order->id) }}" class="btn btn-warning btn-sm btn-block">Edit</a>
							<form action="{{ route('orders.destroy', $order->id) }}" method="post">
								{{ csrf_field() }}
								<input type="hidden" name="_method" value="DELETE">
								<input type="submit" name="submit" value="Delete" class="btn btn-danger btn-sm btn-block"
											   onclick="if (! confirm('Destroy order?')) return false">
							</form>

					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		</div>
	</div>
	<div class="row">
		<div class="col-md-offset-5">
			{{ $orders->links() }}
		</div>
	</div>
@endif
@endsection
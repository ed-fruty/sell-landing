@extends('layouts.container')

@section('heading')
Create a new order
@endsection

@section('panel')
<form method="post" action="{{ route('orders.store') }}" enctype="multipart/form-data">
		{{ csrf_field() }}

		<div class="form-group">
		    <label for="phone">Phone</label>
		    <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone') }}" placeholder="Enter the phone value">
		</div>
		<div class="form-group">
		    <label for="fio">FIO</label>
		    <input type="text" class="form-control" id="fio" name="fio" value="{{ old('fio') }}" placeholder="Enter the fio value">
		</div>
		<div class="form-group">
		    <label for="comment">Comment</label>
		    <textarea class="form-control" id="comment" name="comment" value="{{ old('comment') }}" placeholder="Enter the comment value"></textarea>
		</div>
		{{--<div class="form-group">--}}
			{{--<label for="product_id">Product</label>--}}
			{{--<select name="product_id" class="form-control" id="product_id">--}}
			{{--@foreach($products as $id => $name)--}}
				{{--<option value="{{ $id }}">{{ $name }}</option>--}}
			{{--@endforeach--}}
			{{--</select>--}}
		{{--</div>--}}
		{{--<div class="form-group">--}}
		    {{--<label for="amount">Amount</label>--}}
		    {{--<input type="text" class="form-control" id="amount" name="amount" value="{{ old('amount') }}" placeholder="Enter the amount value. Example: 0.5">--}}
		{{--</div>--}}
		
		<div class="form-group">
			<label for="status">Status</label>
			<select name="status" class="form-control" id="status">
			@foreach($status->dropdown() as $id => $name)
				<option value="{{ $id }}">{{ $name }}</option>
			@endforeach
			</select>
		</div>
		<div class="form-group">
			<div class="row product-block">
				<div class="col-md-6">
					<div class="form-group">
						<label>Product</label>
						<select name="products[]" class="form-control">
							@foreach($products as $id => $name)
								<option value="{{ $id }}">{{ $name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Amount</label>
						<input type="text" name="amounts[]" class="form-control" placeholder="1">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label></label>
						<a href="#" class="btn btn-warning btn-remove">Delete</a>
					</div>

				</div>
			</div>
		</div>

		<div class="form-group">
			<button class="btn btn-primary btn-clone">+ Add a new one</button>
		</div>

		<div class="form-group">
		    <input type="submit" class="btn btn-info btn-block btn-sm" value="Create">
		</div>
</form>
@endsection

@section('resources')
	<script>
        $(function() {
            $('.btn-clone').on('click', function(e) {
                e.preventDefault();

                const
					templateSelector = '.product-block',
                 	content = $(templateSelector).first().clone();

                $(templateSelector).parent().append(content);
            })

			$('body').on('click', '.btn-remove', function(e) {
			    e.preventDefault();

			    if ($('.product-block').length == 1) {
			        return false;
				}

				$(this).closest('.product-block').remove();
			})
        })
	</script>
@endsection
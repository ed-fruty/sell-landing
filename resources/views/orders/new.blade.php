<h1>New order was created</h1>
Quick information : 

<table>
	<thead>
		<tr>ID</tr>
		<tr>Phone</tr>
		<tr>FIO</tr>
		<tr>Comment</tr>
	</thead>
	<tbody>
		<tr>
			<td> {{ $order->id }}</td>
			<td> {{ $order->phone }}</td>
			<td> {{ $order->fio }}</td>
			<td> {{ $order->comment }}</td>
		</tr>
	</tbody>
</table>

<a href="{{ route('orders.edit', $order->id) }}">Click here to get more information</a>
@extends('layouts.container')

@section('heading')
Create a new category
@endsection

@section('panel')
<form method="post" action="{{ route('categories.store') }}" enctype="multipart/form-data">
		{{ csrf_field() }}

		<div class="form-group">
    		<label for="name">Name</label>
    		<input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="Enter the name value">
		</div>
		<div class="form-group">
			<label for="status">Status</label>
			<select name="status" class="form-control" id="status" value="{{ old('status') }}">
			@foreach($status->dropdown() as $id => $name)
				<option value="{{ $id }}">{{ $name }}</option>
			@endforeach
			</select>
		</div>
		<div class="form-group">
		    <label for="image">Image <small>(only jpg/gif/png extensions are supported)</small></label>
		    <input type="file" class="form-control" id="image" name="image" value="{{ old('image') }}" placeholder="Enter the image value">
		</div>
		<div class="form-group">
		    <input type="submit" class="btn btn-info btn-block btn-sm" value="Create">
		</div>
</form>		
@endsection
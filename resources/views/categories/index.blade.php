@extends('layouts.container')

@section('heading')
Categories
@endsection

@section('panel')

<div class="row" style="margin-bottom: 10px">
	<div class="col-md-6">
		<a href="{{ route('categories.create') }}" class="btn btn-info btn-block btn-sm">Create a new category</a>
	</div>
</div>
<div>
@if($categories->isEmpty())
	<div class="alert alert-warning">
		No available categories was found.
	</div>
@else
<div class="row">
	<div class="col-md-12">
	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Image</th>
				<th>Status</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			@foreach($categories as $category)
			<tr>
				<td>{{ $category->id }}</td>
				<td>{{ $category->name }}</td>
				<td>
					@if($category->image)
						<img src="{{ $category->image->getFullPath() }}" width="120" height="100">
					@else
						None
					@endif
				</td>
				<td>
					{{ $status->getStatusName($category->status) }}
				</td>
				<td>
						<form action="{{ route('categories.destroy', $category->id) }}" method="post">
							{{ csrf_field() }}
							<input type="hidden" name="_method" value="DELETE">
							
							<div class="btn-group">
								<a href="{{ route('categories.edit', $category->id) }}" class="btn btn-warning btn-sm">Edit</a>
								<input type="submit" name="submit" value="Delete" class="btn btn-danger btn-sm" 
									onclick="if (! confirm('Destroy category?')) return false">
							</div>
						</form>
					
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	</div>
</div>
<div class="row">
	<div class="col-md-offset-5">
		{{ $categories->links() }}
	</div>
</div>
@endif
@endsection
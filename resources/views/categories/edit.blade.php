@extends('layouts.container')

@section('heading')
Edit category "{{ $category->name }}"
@endsection

@section('panel')
<form method="post" action="{{ route('categories.update', $category->id) }}" enctype="multipart/form-data">
		{{ csrf_field() }}
		<input type="hidden" name="_method" value="PUT">

		<div class="form-group">
    		<label for="name">Name</label>
    		<input type="text" class="form-control" id="name" name="name" value="{{ $category->name }}" placeholder="Enter the name value">
		</div>
		<div class="form-group">
			<label for="status">Status</label>
			<select name="status" class="form-control" id="status">
			@foreach($status->dropdown() as $id => $name)
				<option value="{{ $id }}" {{ $category->status === $id ? 'selected' : '' }} >{{ $name }}</option>
			@endforeach
			</select>
		</div>
		<div class="form-group">
		    <label for="image">Image <small>(only jpg/gif/png extensions are supported)</small></label>
		    <input type="file" id="image" name="image">

		    @if($category->image)
		    	<div style="margin:5px">
		    		<img class="img-rounded" src="{{ $category->image->getFullPath() }}" width="120" height="100">
		    	</div>
		    @endif
		</div>
		<div class="form-group">
		    <input type="submit" class="btn btn-info btn-block btn-sm" value="Save">
		</div>
</form>		
@endsection
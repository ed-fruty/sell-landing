@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
        @if(! $errors->isEmpty())
            <div class="alert alert-danger">
                <ul>
                @foreach($errors->all() as $message)
                    <li>{{ $message }}</li>
                @endforeach
                </ul>
            </div>
        @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@yield('heading', 'Dashboard')</div>

                <div class="panel-body">
                        @section('panel')
                            Welcome to admin panel
                        @show
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
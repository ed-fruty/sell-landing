<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <title>{{ config('app.name') }}</title>

    <meta name="google-site-verification" content="ryhsYpSdJKUBPzE9kk24LloNqB7SvLxEwk17dUKY1Q8" />
	<meta name="yandex-verification" content="54f03fe03e0babe8" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">

    <!-- css -->
    <link rel="stylesheet" href="/theme/css/bootstrap.min.css">
    <link rel="stylesheet" href="/theme/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/theme/css/font-awesome.min.css">
    <link rel="stylesheet" href="/theme/css/main.css">



    <!-- google font -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Kreon:300,400,700'>

    <!-- js -->
    <script src="/theme/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body data-spy="scroll" data-target="#navbar" data-offset="120">
    <div id="app">
    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div id="menu" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header visible-xs">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><h2>{{ config('app.name') }}</h2></a>
            </div><!-- navbar-header -->
        <div id="navbar" class="navbar-collapse collapse">
            {{--<div class="hidden-xs" id="logo"><a href="#header">--}}
                {{--<img src="/theme/img/logo.png" alt="">--}}
            {{--</a></div>--}}

            <ul class="nav navbar-nav navbar-right">
                <li><a href="#story">О нас</a></li>
                <li><a href="#reservation">Заказать</a></li>
                {{--<li><a href="#chefs">Our Chefs</a></li>--}}


                {{--<li><a href="#facts">Статистика</a></li>--}}
                <li><a href="#food-menu">Ассортимент</a></li>
                <li><a href="#special-offser">Каталог</a></li>

                <!--fix for scroll spy active menu element-->
                <li style="display:none;"><a href="#header"></a></li>

            </ul>
        </div><!--/.navbar-collapse -->
        </div><!-- container -->
    </div><!-- menu -->

    <div id="header">
        <div class="bg-overlay"></div>
        <div class="center text-center">
            <div class="banner">
                <h1 class="">{{ config('app.name') }}</h1>
            </div>
            <div class="subtitle"><h4>дополни свой рацион незаменимыми компонентами</h4></div>
        </div>
        <div class="bottom text-center">
            <a id="scrollDownArrow" href="#"><i class="fa fa-chevron-down"></i></a>
        </div>
    </div>
    <!-- /#header -->
    <div id="story" class="light-wrapper">
        <section class="ss-style-top"></section>
        <div class="container inner">
            <h2 class="section-title text-center">О нас</h2>
            <p class="lead main text-center">Только у нас Вы можете заказать товар по оптовым ценам от 0.5 кг.</p>
            <div class="row text-center story">
                <div class="col-sm-4">
                    <div class="col-wrapper">
                        <div class="icon-wrapper"> <i class="fa fa-pagelines"></i> </div>
                        <h3>Продукция</h3>
                        <p>Продукция от лучших поставщиков, соответствующая самым высоким стандартам качества.</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="col-wrapper">
                        <div class="icon-wrapper"> <i class="fa  fa-money"></i> </div>
                        <h3>Цена</h3>
						<p>Присоединяйтесь к числу наших клиентов и экономьте на закупке. Мы продаем по оптовым ценам от 0.5 кг.</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="col-wrapper">
                        <div class="icon-wrapper"> <i class="fa fa-truck"></i> </div>
                        <h3>Доставка</h3>
                        <p>Доставка в течение 1-2 дней. Самая быстрая доставка, в любую точку Одессы (при заказе от 3кг), а также через Новую Почту - любые объёмы.</p>
                    </div>
                </div>
            </div>
            <!-- /.services -->
        </div>
        <!-- /.container -->
        <section class="ss-style-bottom"></section>
    </div><!-- #story -->


    {{--<div id="facts" class="parallax parallax2 facts">--}}
        {{--<div class="container inner">--}}
            {{--<div class="row text-center services-3">--}}
                {{--<div class="col-sm-3">--}}
                    {{--<div class="col-wrapper">--}}
                    {{--<div class="icon-border bm10"> <i class="fa fa-beer"></i> </div>--}}
                    {{--<h4>796518</h4>--}}
                    {{--<p>Mug of Beer Sold</p>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-sm-3">--}}
                {{--<div class="col-wrapper">--}}
                    {{--<div class="icon-border bm10"> <i class="fa fa-play-circle-o"></i> </div>--}}
                    {{--<h4>39472</h4>--}}
                    {{--<p>Movies Watched</p>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-sm-3">--}}
                {{--<div class="col-wrapper">--}}
                    {{--<div class="icon-border bm10"> <i class="fa fa-truck"></i> </div>--}}
                    {{--<h4>2188764</h4>--}}
                    {{--<p>Pizza Deleverd</p>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-sm-3">--}}
                {{--<div class="col-wrapper">--}}
                    {{--<div class="icon-border bm10"> <i class="fa fa-users"></i> </div>--}}
                    {{--<h4>480523</h4>--}}
                    {{--<p>Customers Worldwide</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!-- /.container --> --}}
    {{--</div><!-- #facts -->--}}




    <div id="food-menu" class="light-wrapper">
        <section class="ss-style-top"></section>
        <div class="container inner">
            <h2 class="section-title text-center">Ассортимент</h2>
            {{--<p class="lead main text-center">There is no sincerer love than the love of food!</p>--}}

                        <div class="row">
                <div class="col-md-3 col-sm-3" v-for="category in categories">
                    <div class="menu-images">
                        <img  :src="'/images/categories/' + category.image.path" :alt="category.name" style="max-width:250px; max-height:90px">
                    </div>
                    <div class="menu-titles"><h1>@{{ category.name }}</h1></div>
                    <div class="menu-items">
                        <ul v-for="product in category.products">
                            <li> @{{ product.name }} </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->
        <section class="ss-style-bottom"></section>
    </div><!--/#food-menu-->




    <div id="special-offser" class="parallax pricing">
        <div class="container inner">

            <h2 class="section-title text-center">Каталог</h2>
            {{--<p class="lead main text-center">There is no sincerer love than the love of food!</p>--}}

            <div class="row">
                <div class="col-md-6 col-sm-6" v-for="(product, index) in products">
                    <div class="pricing-item" :id="'product-'">
                        <a v-on:click="form(product)">
                            <img class="img-responsive img-thumbnail" :src="'/images/products/' + product.image.path" :alt="product.name" style="max-width:250px; max-height:154px">
                        </a>
                        <div class="pricing-item-details">
                            <h3><a v-on:click="form(product)" v-text="product.name"></a></h3>
                            <p v-text="product.description"></p>
                            <strong>Количество (<span>@{{ getProductUnitName(product) }}</span>)</strong>

                            <input type="text" v-model="productAmounts[product.id]" name="amount"
                                   class="form-control" style="background: white; color: black; width: 60px">
                            <br>
                            <a class="btn btn-primary" v-on:click="basket(product.id, productAmounts[product.id], true)">В корзину</a>
                            <a class="btn btn-danger" v-on:click="form(product)">Оформить заказ</a>
                            <div class="clearfix"></div>
                        </div>
                        <span class="hot-tag br-red" v-text="product.price + product.currency"></span>
                        <div class="clearfix"></div>
                    </div>
					<div class="clearfix" v-if="index % 2 === 0"></div>
                </div>
            </div>

        </div>
        <!-- /.container -->
    </div><!-- /#special-offser -->




    <div id="reservation" class="light-wrapper">
        <section class="ss-style-top"></section>
        <div class="container inner">
            <h2 class="section-title text-center">Оформление заказа</h2>
            <p class="lead main text-center">Закажи свою долю орешков.</p>
            <div class="row">

                <div class="col-md-6">
                    <form class="form form-table" method="post" name="">
                        <div class="form-group">
                            <h4>Заполните форму заказа (все поля обязательны)</h4>
                        </div>

                        <div class="row">
                          <div class="col-lg-6 col-md-6 form-group">
                            <label for="fio">Ваше имя</label>
                            <input class="form-control hint" v-model="fio" type="text" id="fio" name="fio" placeholder="Иванов Василий" required="">
                          </div>
                          <div class="col-lg-6 col-md-6 form-group">
                            <label for="phone1">Телефон</label>
                            <input class="form-control hint" v-model="phone" type="text" id="phone1" name="phone" placeholder="+380931234567" required="">
                          </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-md-6 form-group">
                                <table class="table" v-if="basketItems">
                                    <thead>
                                        <tr>
                                            <th>Название продукта</th>
                                            <th>Базовая цена</th>
                                            <th>Количество</th>
                                            <th>Суммарно</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(amount, id) in basketItems">
                                            <td>@{{ findProduct(id).name }}</td>
                                            <td>@{{ findProduct(id).price }} @{{ findProduct(id).currency }}</td>
                                            <td>@{{ amount }} @{{ getProductUnitName(findProduct(id)) }}</td>
                                            <td>
                                                @{{ (findProduct(id).price * amount).toFixed(2) }}
                                                @{{ findProduct(id).currency }}
                                            </td>
                                            <td>
                                                <a v-on:click="removeFromBasket(id)" style="cursor: pointer">[X]</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3">Всего</td>
                                            <td colspan="2">
                                                <div v-for="(amount, currency) in getOrderSummaryPrice()">
                                                    @{{ amount }} @{{ currency }}
                                                </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-12 col-md-12 form-group">
                            <label for="comment">Комментарий</label>
                            <textarea class="form-control hint" id="comment" name="comment" v-model="comment"></textarea>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-12 col-md-12">
                            <p>После заказа с Вами свяжется наш менеджер и уточнит детали доставки продукции.</p>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-12 col-md-12">
                            <a v-on:click="order()" class="btn btn-danger btn-lg">Заказать!</a>
                          </div>
                        </div>
                      </form>
                </div><!-- col-md-6 -->
                <div class="col-md-4 col-md-offset-1">

                    <h3><i class="fa fa-clock-o fa-fw"></i>Время работы</h3>
                    <h4>Понедельник-Пятница</h4>
                    <p>8:00 - 21:00</p>

                    <h3><i class="fa fa-mobile fa-fw"></i>Контакты</h3>
                    <p>Email: <a href="mailto:andrew.patsuk@gmail.com">contacts@ohmynuts.com.ua</a></p>
                    <p>Телефон: +38 (063) 063 50 63 </p>
					<p>Телефон: +38 (093) 823 83 59 </p>

                </div><!-- col-md-6 -->
            </div>
            <!-- /.services -->
        </div>
        <!-- /.container -->
        <section class="ss-style-bottom"></section>
    </div><!-- #reservation -->



    {{--<div id="chefs" class="parallax pricing">--}}
        {{--<div class="container inner">--}}

            {{--<h2 class="section-title text-center">Our Chefs</h2>--}}
            {{--<p class="lead main text-center">There is no sincerer love than the love of food!</p>--}}
            {{----}}
            {{--<div class="row text-center chefs">--}}
                {{--<div class="col-sm-4">--}}
                    {{--<div class="col-wrapper">--}}
                        {{--<div class="icon-wrapper">--}}
                            {{--<img src="/theme/img/chefs/1.jpg">--}}
                        {{--</div>--}}
                        {{--<h3>Saransh Goila</h3>--}}
                        {{--<p>Vivamus sagittis lacuson augue laoreet rutrum faucibus dolor auctor. Cras mattis consectetur purus sit amet fermentum ultricies vehicula.</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-sm-4">--}}
                    {{--<div class="col-wrapper">--}}
                        {{--<div class="icon-wrapper">--}}
                            {{--<img src="/theme/img/chefs/3.jpg">--}}
                        {{--</div>--}}
                        {{--<h3>Jane Doe</h3>--}}
                        {{--<p>Nullam id dolor id nibh ultricies vehicula ut id elit. Cum sociis natoque penatibus et magnis dis parturient monte nascetur ultricies vehicula. </p>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-sm-4">--}}
                    {{--<div class="col-wrapper">--}}
                        {{--<div class="icon-wrapper">--}}
                            {{--<img src="/theme/img/chefs/2.jpg">--}}
                        {{--</div>--}}
                        {{--<h3>Anton Mosimann</h3>--}}
                        {{--<p>Curabitur blandit matti tempus porttitor. Donec id elit non mi porta ut gravida at eget metus. Consectetur adipiscing elit ultricies vehicula.</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

        {{--</div>--}}
        {{--<!-- /.container --> --}}
    {{--</div><!-- /#chefs -->--}}


    <footer id="footer" class="dark-wrapper">
        {{--<section class="ss-style-top"></section>--}}
        <div class="container inner">
            <div class="row">
                <div class="col-sm-6">
                    {{--&copy; Copyright MeatKing 2014--}}
                    {{--<br/>Theme By <a class="themeBy" href="http://www.Themewagon.com">ThemeWagon</a>--}}
                </div>
                <div class="col-sm-6">
                    <div class="social-bar">

                        <a target="_blank" href="https://www.instagram.com/ohmynuts_ua/" class="fa fa-instagram tooltipped" title=""></a>
                       <!-- <a href="#" class="fa fa-youtube-square tooltipped" title=""></a>
                        <a href="#" class="fa fa-facebook-square tooltipped" title=""></a>
                        <a href="#" class="fa fa-pinterest-square tooltipped" title=""></a>
                        <a href="#" class="fa fa-google-plus-square tooltipped" title=""></a> -->
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container -->
    </footer>

    <!-- <script src="/js/app.js"></script> -->
    </div>
    </div>
    <script src="/theme/js/jquery-2.1.3.min.js"></script>
    <script src="https://unpkg.com/vue/dist/vue.js"></script>
    <script src="/theme/js/jquery.actual.min.js"></script>
    <script src="/theme/js/jquery.scrollTo.min.js"></script>
    <script src="/theme/js/bootstrap.min.js"></script>
    <script src="/js/main.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
</body>
</html>
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/** @var Router $router */

use Illuminate\Routing\Router;

$router->get('/', function () {
    return view('welcome');
});

// Authentication Routes...
$router->get('login', 'Auth\LoginController@showLoginForm')->name('login')->middleware('throttle:5,1');
$router->post('login', 'Auth\LoginController@login')->middleware('throttle:5,1');
$router->post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
//$router->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//$router->post('register', 'Auth\RegisterController@register');

//$router->get('/fresh-users', function() {
//    App\User::all()->each(function(\App\User $user) {
//        $user->delete();
//    });
//});

// Password Reset Routes...
$router->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
$router->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
$router->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
$router->post('password/reset', 'Auth\ResetPasswordController@reset');



$router->group(['prefix' => 'admin', 'middleware' => 'auth'], function(Router $router) {

	$router->resources([
		'categories'	=> 'CategoriesController',
		'products'		=> 'ProductsController',
		'orders'		=> 'OrdersController',
	]);
});

$router->get('/home', 'HomeController@index');
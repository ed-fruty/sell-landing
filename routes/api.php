<?php

use App\Category;
use App\Common\Status\Status;
use App\Http\Requests\StoreOrderRequest;
use App\Order;
use App\Product;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::group(['middleware' => 'scope.status'], function() {
	
	Route::get('categories', function(Request $request) {
		return Category::with(['image', 'products.image'])->get();
	});

	Route::post('order', function(StoreOrderRequest $request) {
		$order = Order::create(array_merge($request->all(), [
			'status' 	=> Status::STATUS_ACTIVE,
			'comment'	=> $request->get('comment'),
		]));

        foreach ($request->get('products') as $index => $product) {

            $order->products()->attach($product, [
                'amount' => $request->get('amounts')[$index],
            ]);
        }

		return $order->id;
	});
});

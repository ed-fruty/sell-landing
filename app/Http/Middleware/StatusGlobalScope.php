<?php

namespace App\Http\Middleware;

use App\Category;
use App\Common\Status\StatusModelScope;
use App\Product;
use Closure;

class StatusGlobalScope
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $scope = new StatusModelScope;

        Category::addGlobalScope($scope);
        Product::addGlobalScope($scope);

        return $next($request);
    }
}

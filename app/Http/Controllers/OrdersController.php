<?php

namespace App\Http\Controllers;

use App\Common\Search\OrderSearch;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Order;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class OrdersController
{
    /**
     * @var ViewFactory
     */
    private $view;
    /**
     * @var Redirector
     */
    private $redirect;

    /**
     * OrdersController constructor.
     * @param ViewFactory $view
     * @param Redirector $redirect
     */
    public function __construct(ViewFactory $view, Redirector $redirect)
    {
        $this->view = $view;
        $this->redirect = $redirect;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param OrderSearch $orderSearch
     * @return \Illuminate\Http\Response|View
     */
    public function index(Request $request, OrderSearch $orderSearch)
    {
        return $this->view->make('orders.index', [
            'orders'    => $orderSearch->searchByRequest($request),
            'request'   => $request,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response|View
     */
    public function create()
    {
        return $this->view->make('orders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreOrderRequest $request
     * @param Order $model
     * @return \Illuminate\Http\Response|RedirectResponse
     */
    public function store(StoreOrderRequest $request, Order $model)
    {
        /** @var Order $order */
        $order = $model->newQuery()->create($request->all());

        foreach ($request->get('products') as $index => $product) {
            $order->products()->attach($product, [
                'amount' => $request->get('amounts')[$index],
            ]);
        }

        return $request->ajax()
            ?   $order->getKey()
            :   $this->redirect->route('orders.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  Order $order
     * @return Model
     */
    public function show(Order $order)
    {
        return $order;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Order $order
     * @return \Illuminate\Http\Response|View
     */
    public function edit(Order $order)
    {
        return $this->view->make('orders.edit', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateOrderRequest $request
     * @param  Order $order
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOrderRequest $request, Order $order)
    {
        $order->fill($request->all())->save();

        $order->products()->sync([]);

        foreach ($request->get('products') as $index => $product) {

            $order->products()->attach($product, [
                'amount' => $request->get('amounts')[$index],
            ]);
        }

        return redirect()->route('orders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Order $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->delete();

        return redirect()->route('orders.index');
    }
}

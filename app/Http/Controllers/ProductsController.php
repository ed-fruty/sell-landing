<?php

namespace App\Http\Controllers;

use App\Category;
use App\Common\Currency\Currency;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Image;
use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('products.index', [
            'products'  => Product::with('category')->latest()->paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create', [
            'currencies'    => (new Currency())->dropdown(),
            'categories'    => Category::latest()->pluck('name', 'id')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreProductRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $product = Product::create($request->all());

        if ($request->file('image')) {
            $fileName = $product->id . '.' . $request->file('image')->extension();
            $request->file('image')->storeAs('images/products', $fileName);

            $product->image()->create(['path'  => $fileName]);
        }

        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return $product;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('products.edit', [
            'product'       => $product,
            'currencies'    => (new Currency)->dropdown(),
            'categories'    => Category::latest()->pluck('name', 'id'), 
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateProductRequest  $request
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        $product->fill($request->all())->save();

        if ($request->file('image')) {

            $product->image()->get()->each(function(Image $image) {
                $image->delete();
            });

            $fileName = $product->id . '.' . $request->file('image')->extension();
            $request->file('image')->storeAs('images/products', $fileName);

             $product->image()->create(['path'  => $fileName]);
        }

        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('products.index');
    }
}

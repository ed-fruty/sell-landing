<?php

namespace App\Http\Requests;

use App\Common\Status\Status;
use Illuminate\Foundation\Http\FormRequest;

class StoreCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required',
            'status'    => 'required|numeric|in:' . implode(',', array_keys((new Status)->dropdown())),
            'image'     => 'required|image'
        ];
    }
}

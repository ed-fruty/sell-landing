<?php

namespace App\Http\Requests;

use App\Common\Currency\Currency;
use App\Common\Status\Status;
use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required',
            'category_id'   => 'required|exists:categories,id',
            'price'         => 'required|numeric',
            'currency'      => 'required|in:' . implode(',', array_keys((new Currency)->dropdown())),
            'status'        => 'required|in:' . implode(',', array_keys((new Status)->dropdown())),
            'image'         => 'required|image',
        ];
    }
}

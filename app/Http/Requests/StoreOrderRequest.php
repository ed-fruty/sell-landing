<?php

namespace App\Http\Requests;

use App\Common\Status\Status;
use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|regex:~\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})~',
            'fio'   => 'required',
            //'product_id'    => 'exists:products,id',
            //'amount'        => 'required|numeric',
            'status'        => $this->has('status') && $this->user()
                ? ('in:' . implode(',', array_keys((new Status)->dropdown()))) 
                : '',
            'products.*'    => 'required|exists:products,id',
            'amounts.*'     => 'required|numeric',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'phone.required'    => 'Поле телефон является обязательным',
            'phone.regex'       => 'Неверный формат телефона. Пример: +380631234567',
            'fio.required'      => 'Не указано имя',
            //'product_id.exists' => 'Неверный идентификатор товара',
            //'amount.required'   => 'Не указано количество',
            //'amount.numeric'    => 'Неверно указано количество',
            'products.*.required' => 'Должен быть выбран как минимум один товар',
            'products.*.exists' => 'Выбран неверный идентификатор товара',
            'amounts.*.required'=> 'Количество не указано',
            'amounts.*.numeric' => 'Количество указано неверно'
        ];
    }
}

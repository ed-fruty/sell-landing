<?php
namespace App\Common\Search;

use App\Order;
use Illuminate\Http\Request;

/**
 * Class ProductSearch
 * @package App\Common\Search
 */
class OrderSearch
{
    /**
     * @var Order
     */
    private $order;

    /**
     * OrderSearch constructor.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function searchByRequest(Request $request)
    {
        $query = $this->order->newQuery()->with(['products.category'])->latest();

        $searchAttributes = [
            'phone' => 'like',
            'fio'   => 'like',
            'product_id' => 'equals',
            'status'    => 'equals'
        ];

        foreach ($searchAttributes as $attribute => $type) {
            if ($request->has($attribute)) {

                switch ($type) {
                    case 'equals':
                        $query->where($attribute, $request->get($attribute));
                        break;

                    case 'like':
                        $query->where($attribute, 'like', "%{$request->get($attribute)}%");
                        break;
                }

            }
        }

        return $query->paginate();
    }
}
<?php
namespace App\Common;

/**
 * Class Application
 * @package App\Common
 */
class Application extends \Illuminate\Foundation\Application
{
    /**
     * @return string
     */
    public function publicPath()
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'www';
    }
}
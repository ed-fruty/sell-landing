<?php

namespace App\Common\Status;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class StatusModelScope implements Scope
{
	
	public function apply(Builder $builder, Model $model)
	{
		$builder->where('status', Status::STATUS_ACTIVE);
	}
}
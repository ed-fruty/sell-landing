<?php
namespace App\Common\Status;

final class Status
{
	const STATUS_INACTIVE = 0;
	const STATUS_ACTIVE = 1;
	const STATUS_IN_PROCESS = 2;
	const STATUS_DELIVERED = 3;
	const STATUS_CANCELED = 4;


	const STATUS_NAME_INACTIVE 	= 'Inactive';
	const STATUS_NAME_ACTIVE 	= 'Active';
	const STATUS_NAME_IN_PROCESS= 'In process';
	const STATUS_NAME_DELIVERED = 'Delivered';
	const STATUS_NAME_CANCELED	= 'Canceled';
	
	/**
	 * Get status readble value.
	 * 
	 * @param  int $status 
	 * @return string         
	 */
	public function getStatusName($status)
	{
		switch( (int) $status) {
			case self::STATUS_INACTIVE:
				return self::STATUS_NAME_INACTIVE;
			case self::STATUS_ACTIVE:
				return self::STATUS_NAME_ACTIVE;
			case self::STATUS_IN_PROCESS:
				return self::STATUS_NAME_IN_PROCESS;
			case self::STATUS_DELIVERED:
				return self::STATUS_NAME_DELIVERED;
			case self::STATUS_NAME_CANCELED:
				return self::STATUS_NAME_CANCELED;
			default:
				return 'Undefined status';						
		}
	}

	/**
	 * Check is status has active value.
	 * 
	 * @param  int  $status 
	 * @return boolean         
	 */
	public function isActiveStatus($status)
	{
		return (int) $status === self::STATUS_ACTIVE;
	}

	/**
	 * Dropdown list.
	 * 
	 * @return array 
	 */
	public function dropdown()
	{
		return [
			self::STATUS_INACTIVE 	=> self::STATUS_NAME_INACTIVE,
			self::STATUS_ACTIVE 	=> self::STATUS_NAME_ACTIVE,
			self::STATUS_IN_PROCESS => self::STATUS_NAME_IN_PROCESS,
			self::STATUS_DELIVERED 	=> self::STATUS_NAME_DELIVERED,
			self::STATUS_CANCELED 	=> self::STATUS_NAME_CANCELED
		];
	}

	/**
	 * Check that status value is valid.
	 * 
	 * @param  int  $staus 
	 * @return boolean        
	 */
	public function isValid($staus)
	{
		return in_array($status, array_values($this->dropdown()));
	}
}

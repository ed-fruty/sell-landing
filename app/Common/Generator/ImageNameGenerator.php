<?php
namespace App\Common\Generator;

use Illuminate\Http\Request;

class ImageNameGenerator
{
	private $request;

	private $extension;

	private $columnName;

	public function setForceExtension($extension)
	{
		$this->extension = $extension;

		return $this;
	}

	public function setColumnName($columnName)
	{
		$this->columnName = $columnName;

		return $this;
	}

	public function setRequest(Request $request)
	{
		$this->request = $request;

		return $this;
	}

	public function setType($type)
	{
		$this->type = $type;

		return $this;
	}

	public function setPrefix($prefix)
	{
		$this->prefix = $prefix;

		return $this;
	}

	public function generateImageName($type)
	{
		return ;
	}
} 
<?php 
namespace App\Common\Currency;

class Currency
{
	const CURRENCY_USD = '$';
	const CURRENCY_UAH = '₴';
	const CURRENCY_RUB = '₽';

	const CURRENCY_NAME_USD = 'USD';
	const CURRENCY_NAME_UAH = 'UAH';
	const CURRENCY_NAME_RUB = 'RUB';

	/**
	 * Select dropdown list.
	 * 
	 * @return array 
	 */
	public function dropdown()
	{
		return [
			self::CURRENCY_USD => self::CURRENCY_NAME_USD,
			self::CURRENCY_UAH => self::CURRENCY_NAME_UAH,
			self::CURRENCY_RUB => self::CURRENCY_NAME_RUB,
		];
	}
}
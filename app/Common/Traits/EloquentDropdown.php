<?php
namespace App\Common\Traits;

use Illuminate\Database\Eloquent\Model;

/**
 * Trait EloquentDropdown
 * @package App\Common\Traits
 *
 * @param Model $this
 */
trait EloquentDropdown
{
    /**
     * @return array
     */
    public function dropDown()
    {
        return $this->newQuery()->latest()->pluck(
            $this->getDropDownTextColumn(),
            $this->getDropDownValueColumn()
        );
    }

    /**
     * @return mixed
     */
    protected function getDropDownValueColumn()
    {
        return $this->getKeyName();
    }

    /**
     * @return string
     */
    protected function getDropDownTextColumn()
    {
        return 'name';
    }
}
<?php
namespace App\Common\Unit;


/**
 * Class Unit
 * @package App\Common\Unit
 */
class Unit
{
    const UNIT_VALUE_WEIGHT = 1;
    const UNIT_VALUE_PIECE = 2;
    const UNIT_NAME_WEIGHT = 'kg';
    const UNIT_NAME_PIECE = 'unit';

    /**
     * @return array
     */
    public function dropdown()
    {
        return [
            self::UNIT_VALUE_WEIGHT => self::UNIT_NAME_WEIGHT,
            self::UNIT_VALUE_PIECE => self::UNIT_NAME_PIECE
        ];
    }
}
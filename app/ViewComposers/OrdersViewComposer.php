<?php
namespace App\ViewComposers;

use App\Product;
use Illuminate\Contracts\View\View;

/**
 * Class OrdersViewComposer
 * @package App\ViewComposers
 */
class OrdersViewComposer
{
    /**
     * @var Product
     */
    private $product;

    /**
     * OrdersViewComposer constructor.
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('products', $this->product->dropDown());
    }
}
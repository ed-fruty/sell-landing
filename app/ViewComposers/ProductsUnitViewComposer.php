<?php
namespace App\ViewComposers;

use App\Common\Unit\Unit;
use Illuminate\Contracts\View\View;

/**
 * Class ProductsUnitViewComposer
 * @package App\ViewComposers
 */
class ProductsUnitViewComposer
{
    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('unit', new Unit());
    }
}
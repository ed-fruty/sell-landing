<?php
namespace App\ViewComposers;

use App\Common\Status\Status;
use Illuminate\Contracts\View\View;

/**
 * Class StatusViewComposer
 * @package App\ViewComposers
 */
class StatusViewComposer
{
    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('status', new Status());
    }
}

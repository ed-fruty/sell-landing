<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Image extends Model
{

    /**
     * Mass assingment fillable attributes.
     * 
     * @var array
     */
	protected $fillable = ['path'];

    /**
     * Model bootstraping.
     * 
     * @return void 
     */
	public static function boot()
	{
        parent::boot();

		static::deleted(function(Image $image) {

            /**
             * Remove image file
             * @var [type]
             */
            $filesystem = app('filesystem');

            if ($filesystem->exists($image->getFilePath())) {
                $filesystem->delete($image->getFilePath());
            }
		});
	}

	/**
	 * Imagable feature.
	 * 
	 * @return MorphTo 
	 */
    public function imagable()
    {
    	return $this->morphTo();
    }

    /**
     * Get image full path.
     * 
     * @return string 
     */
    public function getFullPath()
    {
    	return "/images/{$this->getBaseNameType()}/{$this->path}";
    }

    /**
     * Get real file path.
     * 
     * @return string 
     */
    public function getFilePath()
    {
        return "/images/{$this->getBaseNameType()}/{$this->path}";
    }

    /**
     * Get image type based on `imagable_type` attribute.
     * 
     * @return string 
     */
    private function getBaseNameType()
    {
        return str_replace('\\', '', Str::snake(Str::plural(class_basename($this->imagable_type))));
    }
}

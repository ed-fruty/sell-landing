<?php

namespace App;

use App\Common\Traits\EloquentDropdown;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class Product extends Model
{
    use EloquentDropdown;

	/**
	 * Mass assignment attributes.
	 * 
	 * @var array
	 */
	protected $fillable = [
		'name', 'category_id', 'status', 'description', 'price', 'currency', 'unit'
	];

	/**
	 * Attributes type casts.
	 * 
	 * @var array
	 */
	protected $casts = [
		'id'	=> 'int',
    	'status'	=> 'int',
    	'category_id'	=> 'int',
		'price'	=> 'float',
        'unit'  => 'int'
	];

	/**
	 * Model bootstrap method.
	 * 
	 * @return void 
	 */
    public static function boot()
    {
    	parent::boot();

    	static::deleted(function($product) {

    		/*
    		 * Delete related images, when product was deleted.
    		 *
    		 * We not sure that it exists, so get images relation as collection
             * And map every image.
             *
             * If image will have required rule it can be changed to
             *
             * $product->image->delete();
    		 */
    		$product->image()->get()->each(function(Image $image) {
    			$image->delete();
    		});

            /*
             * Delete all related orders. 
             */
            $product->orders->each(function(Order $order) {
                $order->delete();
            });
    	});
    }

    /**
     * Product image relation.
     * 
     * @return MorphOne 
     */
    public function image() : MorphOne
    {
    	return $this->morphOne(Image::class, 'imagable');
    }

    /**
     * Product category relation.
     * 
     * @return BelongsTo 
     */
    public function category() : BelongsTo
    {
    	return $this->belongsTo(Category::class);
    }

    /**
     * Product orders relation.
     * 
     * @return BelongsToMany
     */
    public function orders() : BelongsToMany
    {
        return $this->belongsToMany(Order::class);
    }

    /**
     * @param bool $withCurrency
     * @return float
     */
    public function getOrderPrice($withCurrency = false)
    {
        $price = round($this->getAttribute('pivot')->getAttribute('amount') * $this->getAttribute('price'), 2);

        return $withCurrency ? "{$price} {$this->getAttribute('currency')}" : $price;
    }

}

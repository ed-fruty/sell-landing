<?php

namespace App;

use App\Mail\NewOrder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\Mail;

class Order extends Model
{
	/**
	 * Mass assignment fillable attributes.
	 * 
	 * @var array
	 */
    protected $fillable = ['phone', 'fio', 'status', 'comment'];

    /**
     * Attributes casting map.
     * 
     * @var array
     */
    protected $casts = [
    	'id'			=> 'int',
    	'status'		=> 'int'
    ];

    public static function boot()
    {
        parent::boot();

        static::created(function(Order $order) {
            Mail::to(env("ADMIN_ACCOUNT_EMAIL"))->send(new NewOrder($order));
        });

        static::deleted(function(Order $order) {
            $order->products()->sync([]);
        });
    }

//    /**
//     * Order products relation.
//     *
//     * @return BelongsTo
//     */
//    public function product()
//    {
//    	return $this->belongsTo(Product::class);
//    }

    /**
     * Many products relation.
     *
     * @return BelongsToMany
     *
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'orders_products')
            ->withTimestamps()
            ->withPivot('amount');
    }

    /**
     * @return Collection|mixed
     */
    public function getProductsList()
    {
        return $this->getRelation('products')->isEmpty()
            ? new Collection([$this->getRelation('product')])
            : $this->getRelation('products');
    }

    public function getProductsPrice()
    {
        return new OrderPrice($this);
    }
}

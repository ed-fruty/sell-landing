<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class Category extends Model
{
	/**
	 * Mass assignment attributes.
	 * 	
	 * @var array
	 */
    protected $fillable = ['name', 'status'];

    protected $casts = [
    	'id'	=> 'int',
    	'status'	=> 'int',
    ];

    public static function boot()
    {
    	parent::boot();

    	static::deleted(function($category) {

            /*
             * Delete all relalated products
             */
    		$category->products->each(function(Product $product) {
    			$product->delete();
    		});

            /*
             * Delete related images.
             * We not sure that it exists, so get images relation as collection
             * And map every image.
             *
             * If image will have required rule it can be changed to
             *
             * $product->image->delete();
             */
    		$category->image()->get()->each(function(Image $image) {
                $image->delete();
            });
    	});
    }

    /**
     * Category image relation.
     * 
     * @return MorphOne
     */
    public function image()
    {
    	return $this->morphOne(Image::class, 'imagable');
    }

    /**
     * Category products relation.
     * 
     * @return HasMany 
     */
    public function products()
    {
    	return $this->hasMany(Product::class);
    }
}

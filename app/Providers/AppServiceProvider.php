<?php

namespace App\Providers;

use App\Common\Status\Status;
use App\Product;
use App\ViewComposers\OrdersViewComposer;
use App\ViewComposers\ProductsUnitViewComposer;
use App\ViewComposers\StatusViewComposer;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /** @var Factory $view */
        $view = $this->app['view'];

        $view->composer(['orders.*', 'products.*', 'categories.*'], StatusViewComposer::class);
        $view->composer('orders.*', OrdersViewComposer::class);
        $view->composer(['products.create', 'products.edit'], ProductsUnitViewComposer::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

<?php
namespace App;

use Illuminate\Support\Collection;

/**
 * Class OrderPrice
 * @package App
 */
class OrderPrice
{
    /**
     * @var Order
     */
    private $order;

    /**
     * OrderPrice constructor.
     * @param Order $order
     * @internal param Order $this
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return Collection
     */
    public function total()
    {
        $segments = new Collection();

        $this->order->getRelation('products')->each(function(Product $product) use ($segments) {
            $productPrice = $product->getAttribute('price') * $product->getAttribute('pivot')->getAttribute('amount');
            $oldPrice = $segments->get($product->getAttribute('currency'), 0);

            $segments->put($product->getAttribute('currency'), $productPrice + $oldPrice);
        });

        return $segments;
    }
}
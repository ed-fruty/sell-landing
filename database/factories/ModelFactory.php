<?php

use App\Common\Status\Status;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Order::class, function(Faker\Generator $faker) {

	return [
		'phone'	=> $faker->e164PhoneNumber,
		'fio'	=> $faker->name,
		'comment'	=> $faker->text,
		'amount'	=> mt_rand(0,1) == 0 ? 0.5 : 1,
		'status'	=> mt_rand(0,1) == 0 ? Status::STATUS_ACTIVE : Status::STATUS_DELIVERED,
		'product_id'=> 1
	];
});
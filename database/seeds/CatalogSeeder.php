<?php

use App\Category;
use App\Common\Status\Status;
use App\Product;
use App\User;
use Illuminate\Database\Seeder;

class CatalogSeeder extends Seeder
{

    private $categories = [
        ['id'   => 1,  'name'   => 'Hot drinks'], 
        ['id'   => 2,  'name'   => 'Ice drinks'],
        ['id'   => 3,  'name'   => 'Smoothies'],
        ['id'   => 4,  'name'   => 'Deserts']
    ];

    private $products = [
        ['name' => 'Chicken Fried Rice',    'price' => '26', 'currency' => '$'],
        ['name' => 'Hot Fried Chicken',     'price' => '37', 'currency' => '$'],
        ['name' => 'Thi Chicken Momo',      'price' => '54', 'currency' => '$'],
        ['name' => 'Cocktail Sushi',        'price' => '27', 'currency' => '$'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->flush();

        $this->createAdminAccount();

        $this->createCategories();

        $this->createProducts();
    }

    /**
     * Flush all database records.
     */
    private function flush()
    {
        User::all()->each(function(User $user) { 
            $user->delete(); 
        });

        Category::all()->each(function(Category $category) {
            $category->delete();
        });
    }

    /**
     * Create administrator account.
     */
    private function createAdminAccount()
    {
        User::create([
            'name'              => env('ADMIN_ACCOUNT_NAME', 'Administrator'),
            'email'             => env('ADMIN_ACCOUNT_EMAIL', 'admin@admin.com'),
            'password'          => env("ADMIN_ACCOUNT_PASSWORD", bcrypt('admin')),
            'remember_token'    => str_random(10)
        ]);
    }

    private function createCategories()
    {
        foreach ($this->categories as $index => $fixture) {
            $category = Category::create(array_merge($fixture, [
                    'status'    => Status::STATUS_ACTIVE
                ]));

            $category->image()->create(['path' => "{$fixture['id']}.png"]);



            Storage::copy("/theme/img/menu/{$fixture['id']}.png", "/images/categories/{$fixture['id']}.png");
        }
    }

    private function createProducts()
    {
        Category::all()->each(function(Category $category) {
            foreach ($this->products as $index => $fixture) {
                ++$index;

                $product = Product::create(array_merge($fixture, [
                        'category_id' => $category->id,
                        'status'      => Status::STATUS_ACTIVE,
                        'description'   => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                    ]));
                
                $product->image()->create(['path' => "{$product->id}.jpg"]);

                factory(App\Order::class, mt_rand(1, 5))->make()
                    ->each(function($order) use ($product) {
                        $product->orders()->save($order);
                    });
                
                Storage::copy("/theme/img/dish/dish{$index}.jpg", "/images/products/{$product->id}.jpg");
            }
        });
    }
}

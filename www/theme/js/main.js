(function($,sr) {
	// debouncing function from John Hann
	// http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
	var debounce = function (func, threshold, execAsap) {
		var timeout;
		return function debounced () {
			var obj = this, args = arguments;
			function delayed () {
				if (!execAsap)
				                  func.apply(obj, args);
				timeout = null;
			}
			;
			if (timeout)
			              clearTimeout(timeout); else if (execAsap)
			              func.apply(obj, args);
			timeout = setTimeout(delayed, threshold || 100);
		}
		;
	}
	// smartresize 
	jQuery.fn[sr] = function(fn) {
		return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr);
	}
	;
}
)(jQuery,'smartresize');


$(document).ready(function() {
	///////////////////////////////
	// Set Home Slideshow Height
	///////////////////////////////
	function setHomeBannerHeight() {
		var windowHeight = jQuery(window).height();
		jQuery('#header').height(windowHeight);
	}
	///////////////////////////////
	// Center Home Slideshow Text
	///////////////////////////////
	function centerHomeBannerText() {
		var bannerText = jQuery('#header > .center');
		var bannerTextTop = (jQuery('#header').height()/2) - (jQuery('#header > .center').height()/2) - 20;
		bannerText.css('padding-top', bannerTextTop+'px');
		bannerText.show();
	}
	setHomeBannerHeight();
	centerHomeBannerText();
	//Resize events
	jQuery(window).smartresize(function() {
		setHomeBannerHeight();
		centerHomeBannerText();
	});
	
	function scroll() {
		if ($(window).scrollTop() == 0 ) {
			//$('.nav > li').removeClass('active');
			console.log($(window).scrollTop());
		} else {
			
		}
	}
	document.onscroll = scroll;
	var $scrollDownArrow = $('#scrollDownArrow');
	var animateScrollDownArrow = function() {
		$scrollDownArrow.animate( {
			top: 5,
		}
		, 400, "linear", function() {
			$scrollDownArrow.animate( {
				top: -5,
			}
			, 400, "linear", function() {
				animateScrollDownArrow();
			}
			);
		});
	}
	animateScrollDownArrow();
	//Set Down Arrow Button
	jQuery('#scrollDownArrow').click(function(e) {
		e.preventDefault();
		jQuery.scrollTo("#story", 1000, {
			offset:-(jQuery('#header #menu').height()), axis:'y'
		}
		);
	});
	jQuery('.nav > li > a, #logo a').click(function(e) {
		e.preventDefault();

		jQuery.scrollTo(jQuery(this).attr('href'), 400, {
			offset:-(jQuery('#header #menu').height()), axis:'y'
		}
		);
	});


});


const app = new Vue({
	el : "#app",
	data : {
		categories : [],
		products : [],
		amounts : [0.5, 1, 1.5, 2, 2.5, 3, 4, 5, 6, 7, 8, 9, 10],
			amount : 1,
			product_id : 0,
			fio : '',
			phone : '',
			comment : '',

		productAmounts : {},
		basketItems : {}
	},
	created : function() {
		$.when($.getJSON('/api/categories'))
			.then(response => {
				this.categories = response;
				this.categories.map(category => category.products.map(product => {
					product.category = category;

					this.products.push(product);

					this.productAmounts[product.id] = 1;
				}));
			});
	},

	methods : {
		debug: function() {
			console.log(this.categories);
		},

		form: function(product) {

			this.basket(product.id, this.productAmounts[product.id], false);
			//this.product_id = product.id;

			jQuery.scrollTo('#reservation', 400, {
				offset:-(jQuery('#header #menu').height()), axis:'y'
			})
		},


		scrollToProduct: function(id, speed) {
            jQuery.scrollTo('#product-item-' + id, speed || 400, {

                offset:-((jQuery('#header #menu').height()) + 50), axis:'y'
            })
		},

		totalPrice: function() {
			let price = 0;
			let amount = this.amount ? this.amount : 1;
			let currency = '$';

			if (this.product_id) {
				let product = this.products.filter(p => p.id == this.product_id)[0];
				price = product.price;
				currency = product.currency;
			}

			return (Number(price) * Number(amount)).toFixed(2) + ' ' + currency;
		},

        basket: function(id, amount, popup) {
			let product = this.findProduct(id);


            amount = isNaN(parseFloat(amount)) ? 1 : parseFloat(amount.toString().replace(',', '.'));
            if (product.unit == 2) {
            	amount = parseInt(amount);
			}

            this.productAmounts[id] = amount;

            if (this.basketItems[id]) {
                Vue.set(this.basketItems, id, amount + this.basketItems[id]);
			} else {
                Vue.set(this.basketItems, id, amount);
			}

            if (popup) {
                toastr.info('Добавлено в корзину!');
			}
        },

		getProductsArrayList: function() {
			let list = [];
			for (let id in this.basketItems) {
				list.push(id);
			}

			return list;
		},

		getAmountsArrayList: function() {
			let list = [];
			for (let id in this.basketItems) {
				list.push(this.basketItems[id]);
			}

			return list;
		},

		order: function() {

			$.when($.ajax({
					url : '/api/order',
					type: 'post',
			 		data : {
			 			amount : this.amount, 
			 			fio : this.fio, 
			 			phone : this.phone, 
			 			product_id : this.product_id, 
			 			comment : this.comment,
						products: this.getProductsArrayList(),
						amounts : this.getAmountsArrayList(),
			 		},
			 		dataType: 'json'
				}))
				.then(response => {
					alert('Спасибо! В ближайшее время с Вами свяжется наш менеджер.');

					this.phone = '';
					this.fio = '';
					this.amount = '';
					this.comment = '';
					
				}, error => {
					
					message = typeof (error.responseJSON == 'object')
						?	 error.responseJSON[Object.keys(error.responseJSON)[0]][0]
						: 	 'Что-то пошло не так';

					alert('Ошибка!\n' + message);
				})
		},

        findProduct: function(id) {
			return this.products.filter(p => parseInt(p.id) == parseInt(id))[0];
		},

		orderProductPrice: function(productId, amount) {
			return this.findProduct(productId).price * parseInt(amount).toFixed(2);
		},

        getOrderSummaryPrice: function() {
			let segments = {};

			for(let id in this.basketItems) {
				let product = this.findProduct(id);

				if (typeof segments[product.currency] == 'undefined') {
					segments[product.currency] = (parseFloat(this.basketItems[id]) * parseFloat(product.price)).toFixed(2);
				} else {
					segments[product.currency] = ((parseFloat(this.basketItems[id]) * parseFloat(product.price))
					 + parseFloat(segments[product.currency])).toFixed(2);
				}
			}

			return segments;
		},

		removeFromBasket: function(productId) {
			console.log(productId);
			for (let id in this.basketItems) {
				console.log(id, productId);
				if (parseInt(productId) === parseInt(id)) {
					Vue.delete(this.basketItems, id);
				}
			}
		},

        getSelectedProductImagePath: function() {
			return this.products.filter(p => p.id == this.product_id)[0].image.path;
		},

        getProductUnitName: function(product) {
			return product.unit == 1
				?	'кг.'
				:	'шт.';
		}
    }

});